import { getSafeName, deepClone, checkOptions, getSafeValue, isArrayStore, getOptionStore, getOptionName } from "./utils";
export function Offer(target, options) {
    if (checkOptions(options)) {
        throw new Error("invaild store is injected");
    }
    var safeValue = getSafeValue(target, options);
    var safeName = getOptionName(target, options);
    var safeStore = getOptionStore(options);
    if (isArrayStore(safeStore)) {
        for (var i in safeStore) {
            if (safeStore[i].exist(safeName)) {
                throw new Error("it can only provide a unique name");
            }
            safeStore[i].add({
                name: safeName,
                value: deepClone(safeValue)
            });
        }
    }
    else {
        if (safeStore.exist(safeName)) {
            throw new Error("it can only provide a unique name");
        }
        safeStore.add({
            name: safeName,
            value: deepClone(safeValue)
        });
    }
}
export function Provide(options) {
    return function (target) {
        Offer(target, options);
    };
}
export function createToken() {
    var randomNumber1 = Math.random();
    var randomNumber2 = Math.random() * Math.random();
    var randomNumber3 = Math.random() * new Date().getTime();
    return getSafeName(randomNumber1.toString() + "/" + randomNumber2.toString() + "/" + randomNumber3.toString());
}
