import { getSafeName, md5 } from "./utils";
var Store = (function () {
    function Store(name) {
        this.name = name;
        this.items = [];
    }
    Store.prototype.add = function (item) {
        this.items.push(item);
    };
    Store.prototype.exist = function (target) {
        var safeName = getSafeName(target);
        for (var i in this.items) {
            if (this.items[i].name === safeName)
                return true;
        }
        return false;
    };
    Store.prototype.get = function (target, useHash) {
        if (useHash === void 0) { useHash = true; }
        var safeName = useHash ? getSafeName(target) : target;
        for (var i in this.items) {
            if (this.items[i].name === safeName)
                return this.items[i].value;
        }
        return null;
    };
    return Store;
}());
export { Store };
var StoreController = (function () {
    function StoreController() {
        this.stores = [];
    }
    StoreController.prototype.is = function (item) {
        return item instanceof Store;
    };
    StoreController.prototype.add = function (item) {
        this.stores.push(item);
    };
    StoreController.prototype.create = function () {
        return createStore();
    };
    StoreController.prototype.count = function () {
        return this.stores.length;
    };
    StoreController.prototype.exist = function (storeOrName) {
        var name = typeof storeOrName === "object" ? storeOrName.name : storeOrName;
        for (var i in this.stores) {
            if (this.stores[i].name === name)
                return true;
        }
        return false;
    };
    return StoreController;
}());
export var storeController = new StoreController();
export var createStore = function () {
    var hashName = md5("new Store()/atPosition/" + storeController.count());
    var store = new Store(hashName);
    storeController.add(store);
    return store;
};
export var rootStore = createStore();
