var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
import { rootStore } from "./store";
export function Inject(injectTarget, store) {
    var safeStore = store || rootStore;
    var targetValue = safeStore.get(injectTarget);
    if (targetValue === null) {
        throw new Error("No such value has been provided");
    }
    return function (target, propertyKey, paramterIndex) {
        if (!target.__injectedData) {
            target.__injectedData = [];
        }
        target.__injectedData[paramterIndex] = targetValue;
    };
}
export function AutoInject(target) {
    var params = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        params[_i - 1] = arguments[_i];
    }
    var getConstructor = function (target) {
        if (target.prototype && target.prototype.constructor) {
            return target.prototype.constructor;
        }
        else if (target.constructor) {
            return target.constructor;
        }
        return target;
    };
    var getInjectedData = function (target) {
        if (target.__injectedData) {
            return target.__injectedData;
        }
        else if (target.prototype && target.prototype.__injectedData) {
            return target.__injectedData;
        }
        else if (target.prototype &&
            target.prototype.constructor &&
            target.prototype.constructor.__injectedData) {
            return target.prototype.constructor.__injectedData;
        }
        return null;
    };
    var targetCons = getConstructor(target);
    var injectedData = getInjectedData(target);
    if (injectedData === null) {
        return new (targetCons.bind.apply(targetCons, __spread([void 0], params)))();
    }
    return new (targetCons.bind.apply(targetCons, __spread([void 0], injectedData, params)))();
}
