import { Store } from "./store";
export interface ProvideOptions {
    name?: any;
    store?: Store | Store[];
    args?: any[];
}
export interface OfferOptions extends ProvideOptions {
    pure?: boolean;
}
export declare function Offer(target: any, options?: OfferOptions): void;
export declare function Provide(options?: ProvideOptions): (target: any) => void;
export declare function createToken(): string;
