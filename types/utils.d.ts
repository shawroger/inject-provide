import { OfferOptions } from "./provide";
import { Store } from "./store";
export declare function getParameterName(targetFunc: Function): RegExpMatchArray;
export declare function getSafeName(target: any): string;
export declare function deepClone<T>(target: T): T;
export declare function isArrayStore(store: Store | Store[]): store is Store[];
export declare function checkStore(store: Store | Store[]): boolean;
export declare function getSafeValue(target: any, options?: OfferOptions): any;
export declare function checkOptions(options?: OfferOptions): boolean;
export declare function getOptionName(target: any, options?: OfferOptions): string;
export declare function getOptionStore(options?: OfferOptions): Store | Store[];
export declare function provideMethodName(target: any, propertyKey?: string, descriptor?: PropertyDescriptor): string;
export declare function md5(value: string): string;
