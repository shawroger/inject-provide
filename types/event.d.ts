import { Store, StoreItem } from "./store";
interface EventStoreItem {
    target: any;
    args?: any[];
    propertyKey: string;
    tag: string | null;
}
export declare class StoreEmitter {
    private storeItem;
    constructor(storeItem: StoreItem<EventStoreItem>[]);
    emit(...args: any[]): void;
}
export declare class EventController {
    private store;
    constructor(store: Store);
    emit(...args: any[]): void;
    getStore(): Store;
    includeTags(tag: string | string[]): StoreEmitter;
    selectTags(tag: string | null | ((tagName: string | null) => boolean)): StoreEmitter;
    private emitHook;
}
export declare class EventInjector {
    store: Store;
    controller: EventController;
    constructor();
    Subject: (...args: any[]) => (target: any) => void;
    Provide: () => (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
    Tag: (tag?: string | undefined) => (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
}
export declare function createEvent(): EventInjector;
export {};
