import { Store } from "./store";
export declare function Inject(injectTarget: any, store?: Store): (target: any, propertyKey: string, paramterIndex: number) => void;
export declare function AutoInject<T>(target: new (...args: any[]) => T, ...params: any[]): T;
