export interface StoreItem<T> {
    name: string;
    value: T;
}
export declare class Store {
    name: string;
    constructor(name: string);
    items: StoreItem<any>[];
    add(item: StoreItem<any>): void;
    exist(target: any): boolean;
    get(target: any, useHash?: boolean): any;
}
declare class StoreController {
    private stores;
    is(item: Store): boolean;
    add(item: Store): void;
    create(): Store;
    count(): number;
    exist(storeOrName: Store | string | symbol): boolean;
}
export declare const storeController: StoreController;
export declare const createStore: () => Store;
export declare const rootStore: Store;
export {};
